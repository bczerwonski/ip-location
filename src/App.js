import styled from "styled-components";
import { useState, useEffect } from "react";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import List from "./components/List";
import Data from "./components/Data";
import Search from "./components/Search";

export default function App() {
  const [history, setHistory] = useState([]);
  const [user, setUser] = useState();
  const [selected, setSelected] = useState();

  const notify = (message) => toast(message);

  async function fetchData(ip, setFun) {
    const response = await fetch(
      `https://geo.ipify.org/api/v1?apiKey=${process.env.REACT_APP_IPIFY_KEY}${
        ip ? `&domain=${ip}` : ""
      }`
    );
    setFun(await response.json());
  }

  const addSearch = (value) => {
    if (value.success !== false) {
      setHistory((prevState) => [value, ...prevState]);
      setSelected(value);
    } else {
      notify(value.error.info);
    }
  };

  const newSearch = (value) => fetchData(value, addSearch);

  useEffect(() => {
    fetchData(undefined, setUser);
  }, []);

  return (
    <Wrapper>
      <List items={history} onClick={(value) => setSelected(value)} />
      <Data value={user} />
      <Search onSubmit={newSearch} />
      <Data value={selected} />
      <ToastContainer />
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 2fr;
  grid-template-rows: 1fr min-content 1fr;

  height: 100vh;
  width: 100vw;
`;
