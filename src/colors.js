export default {
  dark: "#2c3e50",
  gray: "#bdc3c7",
  bacgdround: "#ecf0f1",
  error: "#e74c3c",
  success: "#2ecc71",
  primary: "#f1c40f",
};
