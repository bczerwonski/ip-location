import styled from "styled-components";
import GoogleMapReact from "google-map-react";
import colors from "../colors";

export default function Data({ children, value }) {
  return (
    <Wrapper>
      <GoogleMapReact
        bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLEMAP_KEY }}
        defaultCenter={{
          lat: 52.2326063,
          lng: 20.7810167,
        }}
        center={value && { lat: value.location.lat, lng: value.location.lng }}
        defaultZoom={11}
      ></GoogleMapReact>
      <Info>
        <h1>{value ? value.ip : "n/a"}</h1>

        <div>
          <h4>COUNTRY:</h4>
          {value && value.location.country}
        </div>

        <div>
          <h4>REGION:</h4>
          {value && value.location.region}
        </div>

        <div>
          <h4>CITY:</h4>
          {value && value.location.city}
        </div>
      </Info>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;
`;

const Info = styled.div`
  padding: 16px;
  color: ${colors.dark};
  color: ${colors.dark};
  min-width: 256px;
`;

const Flag = styled.img`
  width: 1em;
`;
