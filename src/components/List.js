import { useMemo } from "react";
import styled from "styled-components";

import ListItem from "./ListItem";

export default function List({ children, items, onClick }) {
  const itemsList = useMemo(
    () =>
      items.map((item) => (
        <ListItem key={item.ip} value={item} onClick={() => onClick(item)} />
      )),
    [items]
  );

  return <Wrapper>{itemsList}</Wrapper>;
}

const Wrapper = styled.div`
  grid-row-end: span 3;
  overflow-y: auto;
`;
