import { Formik, Form, Field } from "formik";
import styled from "styled-components";

import Button from "./form/Button";
import Input from "./form/Input";

import { combine, isRequired, isIPorURL } from "../validate";

export default function Search({ children, onSubmit }) {
  return (
    <Formik
      initialValues={{ value: "" }}
      onSubmit={(value, actions) => onSubmit(value.value)}
    >
      <Wrapper>
        <Field
          name="value"
          component={Input}
          placeholder="IP or URL"
          validate={combine([isRequired, isIPorURL])}
        />
        <Button type="submit">Search</Button>
      </Wrapper>
    </Formik>
  );
}

const Wrapper = styled(Form)`
  display: flex;
  align-items: center;
`;
