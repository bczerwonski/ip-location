import styled from "styled-components";

import colors from "../../colors";

export default function Button({ children, ...props }) {
  return <Wrapper {...props}>{children}</Wrapper>;
}

const Wrapper = styled.button`
  background-color: ${colors.primary};
  color: ${colors.dark};
  border: none;
  font-weight: bold;
  font-size: large;
  padding: 16px 32px;
  margin: 8px;
  cursor: pointer;

  transition: opacity 0.25s;
  opacity: 0.9;

  &:hover {
    opacity: 1;
  }
  &:active {
    opacity: 0.8;
  }
  &:disabled {
    background-color: ${colors.gray};
    cursor: not-allowed;
  }
`;
