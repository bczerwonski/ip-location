import styled from "styled-components";
import colors from "../../colors";

export default function Input({ field, form: { touched, errors }, ...props }) {
  return (
    <Wrapper>
      {errors.value && <Error>{errors.value}</Error>}
      <Field type="text" {...field} {...props} error={errors.value} />
    </Wrapper>
  );
}

const Wrapper = styled.div`
  width: 100%;
  margin: 8px;
  position: relative;
`;

const Field = styled.input`
  color: ${colors.dark};
  outline: none;
  border: none;
  border-bottom: 2px ${colors.gray} solid;
  font-size: large;
  padding: 16px;
  width: 100%;

  transition: border-color 0.1s;

  &:hover,
  &:active,
  &:focus {
    border-bottom-color: ${colors.primary};
  }
`;

const Error = styled.div`
  color: ${colors.error};
  position: absolute;
  top: 0;
  right: 0;

  @keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  animation: fadein 0.25s;
`;
