import styled from "styled-components";

import colors from "../colors";

export default function ListItem({ children, value, onClick }) {
  return (
    <Wrapper onClick={onClick}>
      <h2>{value.ip}</h2>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  color: ${colors.dark};
  border-bottom: 2px ${colors.gray} solid;
  margin: 8px;
  padding: 16px;
  cursor: pointer;

  transition: border-bottom-color 0.1s, color 0.1s;

  &:hover,
  &:active,
  &:focus {
    border-bottom-color: ${colors.primary};
    color: ${colors.primary};
  }
`;
