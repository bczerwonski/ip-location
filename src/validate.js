export const combine = (rules) => (value) => {
  for (const rule of rules) {
    const error = rule(value);
    if (error) return error;
  }
};

export const isRequired = (value) => {
  if (!value) return "Value is required";
};

export const isIP = (value) => {
  if (!/^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)(\.(?!$)|$)){4}$/g.test(value))
    return "Incorect ip";
};

export const isURL = (value) => {
  if (
    !/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/g.test(
      value
    )
  )
    return "Incorect url";
};

export const isIPorURL = (value) => {
  if (isIP(value) && isURL(value)) return "Incorrect url or ip";
};
